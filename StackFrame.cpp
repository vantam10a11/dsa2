#include "StackFrame.h"
#include <iostream>
#include <fstream>
#include "errors.h"
#include "constants.h"
using namespace std;

StackFrame::StackFrame() : opStackMaxSize(OPERAND_STACK_MAX_SIZE), localVarSpaceSize(LOCAL_VARIABLE_SPACE_SIZE), top(-1), root(NULL), count(0) {}

int StackFrame::height(Node *N)
{
    if (N == NULL)
        return 0;
    return N->height;
}

Node *StackFrame::newNode(string key, elementStackFrame localVar, Node *parentNode)
{
    count++;

    Node *node = new Node();
    node->key = key;
    node->localVar = localVar;
    node->left = NULL;
    node->right = NULL;
    node->parent = parentNode;
    node->height = 1; // new node is initially

    return node;
}

Node *StackFrame::rightRotate(Node *y)
{
    Node *x = y->left;
    Node *T2 = x->right;

    // Perform rotation
    x->right = y;
    x->parent = y->parent;
    y->left = T2;
    y->parent = x;
    // Update heights
    y->height = max(height(y->left),
                    height(y->right)) +
                1;
    x->height = max(height(x->left),
                    height(x->right)) +
                1;
    // Return new root
    return x;
}

Node *StackFrame::leftRotate(Node *x)
{
    Node *y = x->right;
    Node *T2 = y->left;

    // Perform rotation
    y->left = x;
    y->parent = x->parent;
    x->right = T2;
    x->parent = y;

    // Update heights
    x->height = max(height(x->left),
                    height(x->right)) +
                1;
    y->height = max(height(y->left),
                    height(y->right)) +
                1;
    // Return new root
    return y;
}

int StackFrame::getBalance(Node *N)
{
    if (N == NULL)
        return 0;
    return height(N->left) - height(N->right);
}

Node *StackFrame::insert(Node *node, string key, elementStackFrame localVar, Node *parentNode)
{
    /* 1. Perform the normal BST insertion */
    if (node == NULL)
    {
        return newNode(key, localVar, parentNode);
    }

    int result = key.compare(node->key);
    if (result < 0)
        node->left = insert(node->left, key, localVar, node);
    else if (result > 0)
        node->right = insert(node->right, key, localVar, node);
    else
    {
        node->localVar = localVar;
        return node;
    }
    /* 2. Update height of this ancestor node */
    node->height = 1 + max(height(node->left),
                           height(node->right));
    /* 3. Get the balance factor of this ancestor
        node to check whether this node became
        unbalanced */
    int balance = getBalance(node);

    // If this node becomes unbalanced, then
    // there are 4 cases

    if (balance > 1 || balance < -1)
    {
        if (balance > 1)
        {
            int compareToLeft = key.compare(node->left->key);
            // Left Left Case
            if (balance > 1 && compareToLeft < 0)
                return rightRotate(node);
            // Left Right Case
            if (balance > 1 && compareToLeft > 0)
            {
                node->left = leftRotate(node->left);
                return rightRotate(node);
            }
        }
        if (balance < -1)
        {
            int compareToRight = key.compare(node->right->key);

            // Right Right Case
            if (balance < -1 && compareToRight > 0)
                return leftRotate(node);

            // Right Left Case
            if (balance < -1 && compareToRight < 0)
            {
                node->right = rightRotate(node->right);
                return leftRotate(node);
            }
        }
    }
    /* return the (unchanged) node pointer */
    return node;
}

bool StackFrame::isFull()
{
    return (top >= (opStackMaxSize - 2)); // 1 element = 2 slot
}

bool StackFrame::isEmpty()
{
    return (top < 1); // 1 element = 2 slot
}

float StackFrame::topStack(int line)
{
    if (isEmpty())
    {
        throw StackEmpty(line); // pass argument line
    }
    else
    {
        return opStack[top - 1]; // print the value, not the type code
    }
};
// when push, always check isFull() first
void StackFrame::push(float val, char type, int line)
{
    if (isFull())
    {
        throw StackFull(line); // pass argument line
    }
    else
    {
        opStack[++top] = val;
        opStack[++top] = type == 'f' ? 1 : 0;
    }
}

elementStackFrame StackFrame::pop(int line)
{
    if (isEmpty())
    {
        throw StackEmpty(line);
    }
    else
    {
        float value, type;
        value = opStack[top - 1];
        type = opStack[top];
        top = top - 2;
        return elementStackFrame{value, type};
    }
}

bool StackFrame::isMismatchType2Op(float typeOp1, float typeOp2, char type, int line)
{
    if (typeOp1 == 1 || typeOp2 == 1)
    {
        if (type == 'i')
        {
            throw TypeMisMatch(line);
        }
        else
            return false;
    }
    else
    {
        if (type == 'i')
        {
            return false;
        }
        else
            throw TypeMisMatch(line);
    }
};

bool StackFrame::isMismatchType1Op(float typeOp, float type, int line)
{
    if (typeOp == type)
    {
        return false;
    }
    else
    {
        throw TypeMisMatch(line);
    }
};

float s2f(std::string str)
{
    return std::stof(str);
};

Node *StackFrame::search(Node *root, string key)
{
    if (root == NULL)
    {
        return NULL;
    }
    else
    {
        int result = key.compare(root->key);
        if (result < 0)
            return search(root->left, key);
        else if (result > 0)
            return search(root->right, key);
        else // Equal keys
        {
            return root;
        }
    }
}

void StackFrame::run(string filename)
{
    // read file
    std::ifstream testCaseFile(filename);
    if (testCaseFile.is_open())
    {
        std::string line;
        int lineNo = 0;
        while (std::getline(testCaseFile, line))
        {
            lineNo++;
            int indexSpace = line.find(" ");
            std::string instruction;
            const int MAX_LOCAL = LOCAL_VARIABLE_SPACE_SIZE / 2;
            if (indexSpace == -1) // 1 param
            {
                std::string whitespaces(" \t\f\v\n\r");
                std::size_t found = line.find_last_not_of(whitespaces);
                if (found != std::string::npos)
                {
                    instruction = line.substr(0, found + 1);
                }
                else
                {
                    instruction = line;
                }
                if (instruction == "top")
                {
                    std::cout << topStack(lineNo) << endl;
                }
                else if (instruction == "iadd")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'i', lineNo))
                    {
                        push(newElement1st.value + newElement2nd.value, 'i', lineNo);
                    }
                }
                else if (instruction == "fadd")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'f', lineNo))
                    {
                        push(newElement1st.value + newElement2nd.value, 'f', lineNo);
                    }
                }
                else if (instruction == "isub")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'i', lineNo))
                    {
                        push(newElement1st.value - newElement2nd.value, 'i', lineNo);
                    }
                }
                else if (instruction == "fsub")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'f', lineNo))
                    {
                        push(newElement1st.value - newElement2nd.value, 'f', lineNo);
                    }
                }
                else if (instruction == "idiv")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'i', lineNo))
                    {
                        if (newElement2nd.value == 0)
                        {
                            throw DivideByZero(lineNo);
                        }
                        else
                        {
                            push(int(newElement1st.value / newElement2nd.value), 'i', lineNo);
                        }
                    }
                }
                else if (instruction == "fdiv")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'f', lineNo))
                    {
                        if (newElement2nd.value == 0)
                        {
                            throw DivideByZero(lineNo);
                        }
                        else
                        {
                            push(float(newElement1st.value / newElement2nd.value), 'f', lineNo);
                        }
                    }
                }
                else if (instruction == "irem")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'i', lineNo))
                    {
                        int a = newElement1st.value;
                        int b = newElement2nd.value;
                        if (b == 0)
                        {
                            throw DivideByZero(lineNo);
                        }
                        else
                        {
                            int result = a - int((a / b)) * b;
                            push(result, 'i', lineNo);
                        }
                    }
                }
                else if (instruction == "imul")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'i', lineNo))
                    {
                        push(newElement1st.value * newElement2nd.value, 'i', lineNo);
                    }
                }
                else if (instruction == "fmul")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'f', lineNo))
                    {
                        push(newElement1st.value * newElement2nd.value, 'f', lineNo);
                    }
                }
                else if (instruction == "ineg")
                {
                    elementStackFrame newElement = pop(lineNo);
                    if (!isMismatchType1Op(newElement.type, 0, lineNo))
                    {
                        push(newElement.value * (-1), 'i', lineNo);
                    }
                }
                else if (instruction == "fneg")
                {
                    elementStackFrame newElement = pop(lineNo);
                    if (!isMismatchType1Op(newElement.type, 1, lineNo))
                    {
                        push(newElement.value * (-1), 'f', lineNo);
                    }
                }
                else if (instruction == "iand")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'i', lineNo))
                    {
                        push(int(newElement1st.value) & int(newElement2nd.value), 'i', lineNo);
                    }
                }
                else if (instruction == "ior")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'i', lineNo))
                    {
                        push(int(newElement1st.value) | int(newElement2nd.value), 'i', lineNo);
                    }
                }
                else if (instruction == "ieq")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'i', lineNo))
                    {
                        push(newElement1st.value == newElement2nd.value ? 1 : 0, 'i', lineNo);
                    }
                }
                else if (instruction == "feq")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'f', lineNo))
                    {
                        push(newElement1st.value == newElement2nd.value ? 1 : 0, 'i', lineNo);
                    }
                }
                else if (instruction == "ineq")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'i', lineNo))
                    {
                        push(newElement1st.value != newElement2nd.value ? 1 : 0, 'i', lineNo);
                    }
                }
                else if (instruction == "fneq")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'f', lineNo))
                    {
                        push(newElement1st.value != newElement2nd.value ? 1 : 0, 'i', lineNo);
                    }
                }
                else if (instruction == "ilt")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'i', lineNo))
                    {
                        push(newElement1st.value < newElement2nd.value ? 1 : 0, 'i', lineNo);
                    }
                }
                else if (instruction == "flt")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'f', lineNo))
                    {
                        push(newElement1st.value < newElement2nd.value ? 1 : 0, 'i', lineNo);
                    }
                }
                else if (instruction == "igt")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'i', lineNo))
                    {
                        push(newElement1st.value > newElement2nd.value ? 1 : 0, 'i', lineNo);
                    }
                }
                else if (instruction == "fgt")
                {
                    elementStackFrame newElement2nd = pop(lineNo);
                    elementStackFrame newElement1st = pop(lineNo);
                    if (!isMismatchType2Op(newElement1st.type, newElement2nd.type, 'f', lineNo))
                    {
                        push(newElement1st.value > newElement2nd.value ? 1 : 0, 'i', lineNo);
                    }
                }
                else if (instruction == "ibnot")
                {
                    elementStackFrame newElement = pop(lineNo);
                    if (!isMismatchType1Op(newElement.type, 0, lineNo))
                    {
                        push(newElement.value == 0 ? 1 : 0, 'i', lineNo);
                    }
                }
                else if (instruction == "i2f")
                {
                    elementStackFrame newElement = pop(lineNo);
                    if (!isMismatchType1Op(newElement.type, 0, lineNo))
                    {
                        push(float(newElement.value), 'f', lineNo);
                    }
                }
                else if (instruction == "f2i")
                {
                    elementStackFrame newElement = pop(lineNo);
                    if (!isMismatchType1Op(newElement.type, 1, lineNo))
                    {
                        push(int(newElement.value), 'i', lineNo);
                    }
                }
            }
            else // 2 param
            {
                instruction = line.substr(0, indexSpace);
                std::string val = line.substr(indexSpace + 1, line.length() - indexSpace - 1);
                if (instruction == "iconst")
                {
                    push(s2f(val), 'i', lineNo);
                }
                else if (instruction == "fconst")
                {
                    push(s2f(val), 'f', lineNo);
                }
                else if (instruction == "istore") // CHECK COUNT < max, key exist just update that node
                {
                    elementStackFrame newElement = pop(lineNo);
                    if (!isMismatchType1Op(newElement.type, 0, lineNo)) // throw when cast to boolen work
                    {
                        if (count < MAX_LOCAL)
                        {
                            root = insert(root, val, newElement, NULL);
                        }
                        else
                            throw LocalSpaceFull(lineNo);
                    }
                }
                else if (instruction == "fstore")
                {
                    elementStackFrame newElement = pop(lineNo);
                    if (!isMismatchType1Op(newElement.type, 1, lineNo)) // throw when cast to boolen wwork
                    {
                        if (count < MAX_LOCAL)
                        {
                            root = insert(root, val, newElement, NULL);
                        }
                        else
                            throw LocalSpaceFull(lineNo);
                    }
                }
                else if (instruction == "iload") // update COUNT
                {                                // SEARCH NOT FOUND
                    if (search(root, val) == NULL)
                    {
                        throw UndefinedVariable(lineNo);
                    }
                    else
                    { // check type mismatch
                        Node *searchNode = search(root, val);
                        if (searchNode->localVar.type != 0)
                        {
                            throw TypeMisMatch(lineNo);
                        }
                        else
                        {
                            push(searchNode->localVar.value, 'i', lineNo);
                        }
                    }
                }
                else if (instruction == "fload")
                {
                    if (search(root, val) == NULL)
                    {
                        throw UndefinedVariable(lineNo);
                    }
                    else
                    { // check type mismatch
                        Node *searchNode = search(root, val);
                        if (searchNode->localVar.type != 1)
                        {
                            throw TypeMisMatch(lineNo);
                        }
                        else
                        {
                            push(searchNode->localVar.value, 'f', lineNo);
                        }
                    }
                }
                else if (instruction == "val")
                {
                    if (search(root, val) == NULL)
                    {
                        throw UndefinedVariable(lineNo);
                    }
                    else
                    {
                        Node *searchNode = search(root, val);
                        std::cout << searchNode->localVar.value << endl; // float format
                    }
                }
                else if (instruction == "par") // cout name of the parent node variable of the variable var in the local variable space + endl
                {
                    if (search(root, val) == NULL)
                    {
                        throw UndefinedVariable(lineNo);
                    }
                    else
                    {
                        Node *searchNode = search(root, val);
                        if (searchNode->parent == NULL)
                        {
                            std::cout << "null" << endl; // float format
                        }
                        else
                        {
                            std::cout << searchNode->parent->key << endl; // float format
                        }
                    }
                }
            }
        }
    }
    else
        std::cout << "Unable to open file";
}