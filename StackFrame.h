#ifndef __STACK_FRAME_H__
#define __STACK_FRAME_H__

#include <string>
#include "constants.h"

struct elementStackFrame
{
    float value, type;
};
// An AVL tree node
class Node
{
public:
    std::string key;
    elementStackFrame localVar;
    Node *left;
    Node *right;
    Node *parent;
    int height;
};

/*
StackFrame declaration
*/
class StackFrame
{

    // class Node;
    int opStackMaxSize;    // max size of operand stack
    int localVarSpaceSize; // size of local variable array
    int top;
    float opStack[OPERAND_STACK_MAX_SIZE];
    Node *root;
    int count;

    // public:
    //     struct elementStackFrame
    //     {
    //         float value, type;
    //     };

public:
    /*
    Constructor of StackFrame
    */
    StackFrame();

    /*
    Run the method written in the testcase
    @param filename name of the file
    */
    void run(std::string filename);

    // method of stack
    void push(float val, char type, int line);
    elementStackFrame pop(int line);
    float topStack(int line);
    bool isEmpty();
    bool isFull();

    // method check error
    bool isMismatchType2Op(float typeOp1, float typeOp2, char type, int line);
    bool isMismatchType1Op(float typeOp, float type, int line);
    bool isLocalSpaceFull(int index);

    // method of AVL
    int height(Node *N);
    Node *newNode(std::string key, elementStackFrame localVar, Node* parentNode);
    Node *rightRotate(Node *y);
    Node *leftRotate(Node *x);
    int getBalance(Node *N);
    Node *insert(Node *node, std::string key, elementStackFrame localVar, Node* parentNode);
    Node *search(Node *root, std::string key);
};

#endif // !__STACK_FRAME_H__